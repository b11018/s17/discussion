// alert('hello.. is it me you\'re looking for?')

// console.log("hi")

// Functions
	// Functions are lines/block of codes that tell our device/application to perform a certain task when called or invoked

// Function Declarations
	/*
		function functionName(){
			code block(statements)
		};
	*/

// a function to print name
	function printName(){
		console.log("My name is JP");
	};

// Function invocation
	printName();
	// result: will run the console.log
	printName();

	// declaredFunction();
	// error: declaredFunction is not defined, you cannot invoke a function unless it is declared

// Function declaration vs. expressions

	declaredFunction();

	function declaredFunction(){
		console.log("Hi, I am from declaredFunction()")
	};

	// Function expressions

	// Anonymous Function - a function without a name
	// variableFunction(); - hoisting

	let variableFunction = function(){
		console.log("Hi I am a variable function")
	};

	variableFunction();

	let funcExpression = function funcName(){
		console.log("Hello from the other side");
	};

	funcExpression();

	// reassign declared function

	declaredFunction = function(){
		console.log("This is an updated declaredFunction");
	};

	declaredFunction();

	// reassignment of a function expression with const is not possible

	const constantFunc = function(){
		console.log("Initialized with const")
	};

	constantFunc();

	// constantFunc = function(){
	// 	console.log("Cannot be reassigned");
	// };

	// constantFunc();
	// result: error

// Function scoping

	{
		let localVar = "Armando Perez"
	};

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	// console.log(localVar) // result - not defined

	function showNames(){

		var functionVar = "Joe"
		let functionLet = "Jane"
		const functionConst = "Jake";

		console.log(functionVar);
		console.log(functionLet);
		console.log(functionConst);
	};

	showNames();

	// console.log(functionVar);
	// console.log(functionLet);
	// console.log(functionConst);
	// error: not defined

// Nested Function

	function myNewFunction(){
		let name = "Yor";

		function nestedFunction(){
			let nestedName = "Brando";
			console.log(nestedName);
		};
		// console.log(nestedName); // result in err
		nestedFunction();
	};

	myNewFunction();

// Function and Global Scoped Variables

	let globalName = "Alex";

	function myNewFunction2(){
		let nameInside = "Marco";

		console.log(globalName);
	};

	myNewFunction2();
	// console.log(nameInside);

// alert()
	/*
		Syntax:
		alert("message");
	*/

	alert("I am here");
	// runs immediately once the page loads

	function showSampleAlert(){
		alert("Hello User!");
	};

	showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.");

// prompt()
	/*
		Syntax:
		prompt("dialogInString")
	*/

	let samplePrompt = prompt("Enter your name");
	console.log(typeof samplePrompt); // result: string

	console.log("Hello " + samplePrompt);

	// if there's no input it will return an empty string
	// if prompt was cancelled, the return will be null

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name");
		let lastName = prompt("Enter your last name");

		console.log("Hello " + firstName + " " + lastName + "!")
		console.log("Welcome to my page");
	}

	printWelcomeMessage();

// Function Naming Message

	// Function should be definitive of its task

	function getCourses(){

		let courses = ["Science 101", "Trigonometry 103", "Physics 107"]
		console.log(courses);
	};
	getCourses();

	// Avoid generic names

	function get(){
		let name = "Edward";
		console.log(name);
	};
	get();

	// Avoid pointless and inappropriate function names

	function foo(){
		console.log(25%5)
	};
	foo();

	// Avoid naming your function in all small/caps. Follow camelCase when naming variables and function

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	};

	displayCarInfo();